import people_data from "../data/people_data.json";

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll({ searchTerm }: { searchTerm?: string }) {
    if (searchTerm) {
      return people_data.filter(
        ({ title, gender, company }) =>
          title === searchTerm ||
          gender === searchTerm ||
          company === searchTerm
      );
    }
    return people_data;
  }
}
