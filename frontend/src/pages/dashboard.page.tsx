import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress, TextField } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [searchValue, setSearchValue] = useState<string>("");

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers({
        searchTerm: searchValue,
      });
      setUsers(result.data);
      setIsLoading(false);
    };
    fetchData();
  }, [searchValue]);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "center" }}>
        {isLoading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <div
              style={{
                marginTop: 24,
              }}
            >
              <TextField
                label="Search"
                value={searchValue}
                onChange={(val) => {
                  const searchValue = val.currentTarget.value;
                  setSearchValue(searchValue);
                }}
              />
            </div>
            <div>
              {users.length
                ? users.map((user) => {
                    return <UserCard key={user.id} {...user} />;
                  })
                : null}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
